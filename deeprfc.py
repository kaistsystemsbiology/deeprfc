"""
Convenience wrapper for running deeprfc.
"""

from deeprfc.__main__ import main

if __name__ == '__main__':
    main()